<?php include('includes/conn.php');?>
<?php 
if ($_SERVER['REQUEST_METHOD']=="POST"){
  
	$u = strip_tags($_POST['user']);
	$p = strip_tags($_POST['pass']);

	if (isset($u,$p)){

	try {
		$str= "select * from tbl_login where user=:u and pass=:p";
		$cm=$conn->prepare($str);
		$cm->bindParam(':u', $u);
		$cm->bindParam(':p', $p);
		$cm->execute();
		$user = $cm->rowcount();
		
		if ($user == 0) {
			echo "login fail";
			header("refresh:1;url=./");
			
		}else{
      session_start();
      $_SESSION['user'] = $u;
      echo $_SESSION['user'] ;
      #die();
      header("refresh:1;url=./admin/");
    die();
  }


	} catch (Exception $e) {
		echo 'error  '.$e ->getmessage();
	}
	
	}

}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Signin</title>

    <!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">

  </head>
   <body class="text-center">
    <form class="form-signin" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="text" name="user" id="inputEmail" class="form-control" placeholder="Username" required autofocus>
	  <br>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-alert btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">Alright Reserved 2018.</p>
    </form>




  </body>
</html>