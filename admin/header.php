<?php
include('../includes/conn.php');
session_start();
if(!isset($_SESSION['user'])){
 #echo $_SESSION['user'];
  header('location:../login.php');
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Event Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Event List</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
			   <a class="nav-link" href="./">
                  Dashboard 
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Events
                </a>
				<ul>
				<li class="nav-item">
					<a class="nav-link" href="list_event.php">
					  List of Events
					</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" href="add_event.php">
					 Add Event
					</a>
				  </li>
				  <!-- <li class="nav-item">
					<a class="nav-link" href="edit_event.php">
					 Edit Event
					</a>
				  </li> -->
			  
				</ul>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="participants.php">
                  Participants
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="categories.php">
                  Category
                </a>
              </li>
			    <li class="nav-item">
                <a class="nav-link" href="voters.php">
                  Voters
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../logout.php">
                  logout
                </a>
              </li>
            </ul>     
          </div>
        </nav>