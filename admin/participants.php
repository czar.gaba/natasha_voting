<?php include 'header.php';?>
<?php 

if($_SERVER['REQUEST_METHOD']=="POST"){
if(isset($_POST['add'])){
//addfn
$upload_dir = '../images/';

$ename = $_POST['ename'];
$edetails  = $_POST['edetails'];
$ecat  = $_POST['ecat'];
$fileName = $_FILES['eimg']['name'];
$tmpName = $_FILES['eimg']['tmp_name'];
$fileSize = $_FILES['eimg']['size'];
$fileType = $_FILES['eimg']['type'];
$filePath =  $upload_dir . $fileName;
$result = move_uploaded_file($tmpName, $filePath);
if(isset($_FILES["eimg"])){

	if($result){

		$str= "INSERT INTO `tbl_participants`( `name`, `image`, `events`, `details`) VALUES (:a , :b , :c , :d)";
		$cm=$conn->prepare($str);
		$cm->bindvalue(':a', $ename);
		$cm->bindvalue(':d', $edetails);
		#$cm->bindvalue(':c', $edate);
		#$cm->bindvalue(':d', $elocation);
		$cm->bindvalue(':b', $fileName);
		$cm->bindvalue(':c', $ecat);
	#$cm->execute();
		if ($cm->execute()){
			
			header("location: participants.php");
		}else{
			die();
		}
	}else{echo"result not executed <br>".$tmpName;}
}else{
	echo"nofile";
	die();}

}elseif(isset($_POST['update'])){}
//updatefn
$upload_dir = '../images/';
$id = $_POST['id'];
$ename = $_POST['ename'];
$edetails  = $_POST['edetails'];
$ecat  = $_POST['ecat'];
$fileName = $_FILES['eimg']['name'];
$tmpName = $_FILES['eimg']['tmp_name'];
$fileSize = $_FILES['eimg']['size'];
$fileType = $_FILES['eimg']['type'];
$filePath =  $upload_dir . $fileName;
$result = move_uploaded_file($tmpName, $filePath);
if(isset($_FILES["eimg"])){

	if($result){

		$str= "UPDATE `tbl_participants` SET `name`=:a,`image`=:b,`events`=:c,`details`=:d WHERE ID=$id";
		$cm=$conn->prepare($str);
		$cm->bindvalue(':a', $ename);
		$cm->bindvalue(':d', $edetails);
		#$cm->bindvalue(':c', $edate);
		#$cm->bindvalue(':d', $elocation);
		$cm->bindvalue(':b', $fileName);
		$cm->bindvalue(':c', $ecat);
	#$cm->execute();
		if ($cm->execute()){
			
			header("location: participants.php");
		}else{
			die();
		}
	}else{echo"result not executed <br>".$tmpName;}
}else{
	echo"nofile";
	die();}

}
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">PARTICIPANTS</h1>
          </div>


					<?php 
		if(isset($_GET['ID'])){
			$id = $_GET['ID'];
			$st="SELECT * FROM `tbl_participants` where ID=$id";
			$cm=$conn->prepare($st);
			$cm->execute();
			while($row = $cm->fetch(PDO::FETCH_ASSOC)){
				$name = $row['name'];
				$details = $row['details'];
				
				
			}
//UPDATE



			?>
<form class="" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post"  enctype="multipart/form-data">
			  <input type="hidden" value="<?php echo $id ;?>"  name="id" >
				<div class="form-group">
				<label for="participant_name">Participant Name</label>
				<input type="text" name="ename" value="<?php echo $name; ?>" class="form-control" id="participant_name">
			  </div>
			  <div class="form-group">
    <label for="exampleFormControlFile1">Featured Image</label>
    <input type="file" name="eimg" class="form-control-file" id="exampleFormControlFile1">
  </div>
   <div class="form-group">
				  <label for="participant details">Details</label>
				  <textarea class="form-control" rows="5" id="details" name="edetails"><?php echo $details;?></textarea>
				</div>
				<div class="form-group">
				  <label for="category">Event</label>
				  <select class="form-control" id="category" name="ecat">
					<?php 
$st = "SELECT * FROM `tbl_event`";
$cm=$conn->prepare($st);
$cm->execute();
while($row = $cm->fetch(PDO::FETCH_ASSOC)){
	?><option value="<?php echo $row['name'];?>"><?php echo $row['name'];?></option><?php
}
?>
					
				  </select>
				 </div>
			  <button type="submit" class="btn btn-info">UPDATE</button>
			</form>
			<br>
			

			<?php 
		}else{

//ADD
?>

<form class="" action="<?php echo $_SERVER['PHP_SELF'];?> " enctype="multipart/form-data" method="post">
			<input type="hidden" name="add" value="add">
			  <div class="form-group">
				<label for="participant_name">Participant Name</label>
				<input type="text" class="form-control" id="participant_name" name="ename">
			  </div>
			  <div class="form-group">
    <label for="exampleFormControlFile1">Featured Image</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="eimg">
  </div>
   <div class="form-group">
				  <label for="participant details">Details</label>
				  <textarea class="form-control" rows="5" id="details" name="edetails"></textarea>
				</div>
				<div class="form-group">
				  <label for="category">Event</label>
				  <select class="form-control" id="category" name="ecat">
					<?php 
$st = "SELECT * FROM `tbl_event`";
$cm=$conn->prepare($st);
$cm->execute();
while($row = $cm->fetch(PDO::FETCH_ASSOC)){
	?><option value="<?php echo $row['name'];?>"><?php echo $row['name'];?></option><?php
}
?>
					
				  </select>
				 </div>
			  <button type="submit" class="btn btn-info">ADD</button>
			</form>
			<br>
			

<?php 
		}
?>

<form>
			  <div class="form-group">
				<input type="text" class="form-control" id="search" placeholder="SEARCH">
			  </div>
			  </form>
			  <div class="table-responsive">
			  <table class="table table-striped table-sm">
					<tr>
						<th>ID</th>
						<th>Participant Name</th>
						<th>Featured Image</th>
						<th>Details</th>
						<th>Event</th>
						<th>Action</th>
					</tr>
					<?php 
					$st = "SELECT * FROM `tbl_participants`";
					$cm=$conn->prepare($st);
					$cm->execute();
					while($row = $cm->fetch(PDO::FETCH_ASSOC)){
						?>
					<tr>
						<td><?php echo $row['ID'];?></td>
						<td><?php echo $row['name'];?></td>
						<td><img src="../images/<?php echo $row['image']; ?>" alt="" class="img-responsive" height="150"></td>
						<td><?php echo $row['details'];?></td>
						<td><?php echo $row['events'];?></td>
						<td><a class="btn btn-warning" style="margin-right:20px;" href="participants.php?ID=<?php echo $row['ID'];?>">EDIT</a><a type="button" class="btn btn-danger" href="?delID=<?php echo  $row['ID']; ?>">DELETE</a></td>
					</tr>
						<?php
					}
					?>
					
					
				</table>
          </div>
</main>



<?php 
if(isset($_GET['delID'])){
	$id=$_GET['delID'];
	$str= "DELETE FROM `tbl_participants` WHERE ID=$id";
	$cm=$conn->prepare($str);
	$cm->execute();
	header("location: participants.php");
}
?>
<?php include 'footer.php';?>