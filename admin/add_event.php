<?php include 'header.php';?>
<?php 

if($_SERVER['REQUEST_METHOD']=="POST"){

	$upload_dir = '../images/';
		
		$ename = $_POST['ename'];
		$edetails  = $_POST['edetails'];
		$edate = $_POST['edate'];
		$edate = htmlentities($edate);
		$edate = date('Y-m-d', strtotime($edate));
		$elocation = $_POST['elocation'];
		$ecat  = $_POST['ecat'];
		

		$fileName = $_FILES['eimg']['name'];
		$tmpName = $_FILES['eimg']['tmp_name'];
		$fileSize = $_FILES['eimg']['size'];
		$fileType = $_FILES['eimg']['type'];
		$filePath =  $upload_dir . $fileName;
		$result = move_uploaded_file($tmpName, $filePath);
		if(isset($_FILES["eimg"])){

			if($result){

				$str= "INSERT INTO `tbl_event`(`name`, `details`, `date`, `location`, `image`, `cat`) VALUES (:a,:b,:c,:d,:e,:f)";
				$cm=$conn->prepare($str);
				$cm->bindvalue(':a', $ename);
				$cm->bindvalue(':b', $edetails);
				$cm->bindvalue(':c', $edate);
				$cm->bindvalue(':d', $elocation);
				$cm->bindvalue(':e', $fileName);
				$cm->bindvalue(':f', $ecat);
			#$cm->execute();
				if ($cm->execute()){
					
					header("location: list_event.php");
				}else{
					die();
				}

			}else{echo"result not executed <br>".$tmpName;}
			

		}else{
			echo"nofile";
			die();}

		
	

}

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">ADD EVENT</h1>
          </div>
			<form  method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
			  <div class="form-group">
				<label for="event_name">Event Name</label>
				<input type="text" class="form-control" id="participant_name" name="ename" required>
			  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Featured Image</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="eimg" required>
  </div>
			  <div class="form-group">
				  <label for="event details">Event Details</label>
				  <textarea class="form-control" rows="5" id="details" name="edetails" required></textarea>
				</div>
				<div class="form-group">
				  <label for="category">Category </label>
				  <select class="form-control" id="category" name="ecat">
					<?php 
$st = "SELECT * FROM `tbl_category`";
$cm=$conn->prepare($st);
$cm->execute();
while($row = $cm->fetch(PDO::FETCH_ASSOC)){
	?><option value="<?php echo $row['cat'];?>"><?php echo $row['cat'];?></option><?php
}
?>
				  </select>
				 </div>
				 <div class="form-group">
					<label for="scheduled_date">Scheduled Date</label>
					<input type="date" class="form-control" id="scheduled_date" name="edate" required>
				  </div>
					<div class="form-group">
					<label for="location">Location</label>
					<input type="text" class="form-control" id="location" name="elocation" required>
				  </div>
			  <button type="submit" value="submit" class="btn btn-info">ADD</button>
			</form>
			
          </div>
</main>

<?php include 'footer.php';?>