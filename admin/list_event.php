<?php include 'header.php';?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <h2>LIST OF EVENTS</h2>
		  <div class="form-group">
				<input type="text" class="form-control" id="search" placeholder="SEARCH">
			  </div>
          <div class="table-responsive">
		  <table class="table table-striped table-sm">
				<tr>
					<th>ID</th>
					<th>Event Name</th>
					<th>Featured Image</th>
					<th>Details</th>
					<th>Scheduled</th>
					<th>Location</th>
					<th></th>
				</tr>

				<?php 
				$st = "SELECT * FROM `tbl_event`";
				$cm=$conn->prepare($st);
				$cm->execute();
				while($row = $cm->fetch(PDO::FETCH_ASSOC)){
					?>
				<tr>
						<td><?php echo $row['ID']; ?></td>
						<td><?php echo $row['name']; ?></td>
						<td><img src="../images/<?php echo $row['image']; ?>" alt="" class="img-responsive" height="150"></td>
						<td><?php echo $row['details']; ?></td>
						<td><?php echo $row['date']; ?></td>
						<td><?php echo $row['location']; ?></td>
						<td><a class="btn btn-warning" style="margin-right:20px;" href="edit_event.php?ID=<?php echo $row['ID'];?>">EDIT</a><a type="button" class="btn btn-danger" href="?ID=<?php echo  $row['ID']; ?>">DELETE</a></td>
				</tr>
					<?php
				}
				?>

			</table>
          </div>
        </main>

		<?php 
if(isset($_GET['ID'])){
	$id=$_GET['ID'];
	$str= "DELETE FROM `tbl_event` WHERE ID=$id";
	$cm=$conn->prepare($str);
	$cm->execute();
	header("location:list_event.php");
}
?>
<?php include 'footer.php';?>
