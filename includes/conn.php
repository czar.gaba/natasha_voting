<?php 
try {
$constring = 'mysql:host=localhost;dbname=voting'; // host and db
$user= 'root'; // define the username
$pass=''; // password

$conn = new PDO($constring, $user, $pass);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
} catch (PDOException $e) {
	$msg = $e -> getmessage();
	echo "ERROR LIST ->".$msg;
	die();
}

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}