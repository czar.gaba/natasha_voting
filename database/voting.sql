-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2018 at 08:44 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `voting`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `ID` int(11) NOT NULL,
  `cat` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`ID`, `cat`) VALUES
(7, 'Sports'),
(8, 'Pageant');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE IF NOT EXISTS `tbl_event` (
  `ID` int(11) NOT NULL,
  `name` text NOT NULL,
  `details` text NOT NULL,
  `date` date NOT NULL,
  `location` text NOT NULL,
  `image` text NOT NULL,
  `cat` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`ID`, `name`, `details`, `date`, `location`, `image`, `cat`) VALUES
(8, 'MR. AMA', 'Search for MR. AMA TUGUEGARAO', '2018-11-18', 'HOTEL CARMELITA', 'ama-logo-png-1.png', 'Pageant'),
(9, 'MS. AMA', 'Searching for Ms. AMA', '2018-11-18', 'HOTEL CARMELITA', 'ama-logo-png-1.png', 'Pageant');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE IF NOT EXISTS `tbl_login` (
  `ID` int(11) NOT NULL,
  `first` text NOT NULL,
  `last` text NOT NULL,
  `pass` text NOT NULL,
  `user` text NOT NULL,
  `email` text NOT NULL,
  `access` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`ID`, `first`, `last`, `pass`, `user`, `email`, `access`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 'admin@admin.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_participants`
--

CREATE TABLE IF NOT EXISTS `tbl_participants` (
  `ID` int(11) NOT NULL,
  `name` text NOT NULL,
  `image` text NOT NULL,
  `events` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_participants`
--

INSERT INTO `tbl_participants` (`ID`, `name`, `image`, `events`, `details`) VALUES
(12, 'MR. AMA 1', 'avatar-placeholder.png', 'MR. AMA', 'BSIT 201\r\n21 years old'),
(13, 'MR. AMA 2', 'avatar-placeholder.png', 'MR. AMA', 'BSIT 202, 22 years old'),
(14, 'MR. AMA 3', 'avatar-placeholder.png', 'MR. AMA', 'HRM 201, 19 years old'),
(15, 'MS. AMA 1', 'avatar-placeholder.png', 'MS. AMA', 'BSIT 201 21 years old'),
(16, 'MS. AMA 2', 'avatar-placeholder.png', 'MS. AMA', 'HRM 201, 19 years old'),
(17, 'MS. AMA 3', 'avatar-placeholder.png', 'MS. AMA', 'BSIT 202, 22 years old');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vote`
--

CREATE TABLE IF NOT EXISTS `tbl_vote` (
  `ID` int(11) NOT NULL,
  `pname` text NOT NULL,
  `event` text NOT NULL,
  `vname` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voter`
--

CREATE TABLE IF NOT EXISTS `tbl_voter` (
  `ID` int(11) NOT NULL,
  `name` text NOT NULL,
  `number` text NOT NULL,
  `image` text NOT NULL,
  `course` text NOT NULL,
  `section` text NOT NULL,
  `year` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_voter`
--

INSERT INTO `tbl_voter` (`ID`, `name`, `number`, `image`, `course`, `section`, `year`) VALUES
(9, 'John Doe', '0001', 'avatar-placeholder.png', 'BSIT', 'A1', '2018'),
(10, 'Maricar Reyes', '00003', 'avatar-placeholder.png', 'HRM', 'B1', '2016'),
(11, 'Emcee Garcia', '122421', 'avatar-placeholder.png', 'BSIT', 'A1', '2017');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_event`
--
ALTER TABLE `tbl_event`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_participants`
--
ALTER TABLE `tbl_participants`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_vote`
--
ALTER TABLE `tbl_vote`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_voter`
--
ALTER TABLE `tbl_voter`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_event`
--
ALTER TABLE `tbl_event`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_participants`
--
ALTER TABLE `tbl_participants`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_vote`
--
ALTER TABLE `tbl_vote`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_voter`
--
ALTER TABLE `tbl_voter`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
